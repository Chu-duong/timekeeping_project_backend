package com.aibou.security.controller;

import com.aibou.security.dto.response.UserResponse;
import com.aibou.security.entity.Employee;
import com.aibou.security.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;
    @GetMapping("/employee")
    public List<Employee> getAll()
    {
        return employeeService.getEmployeeAll();
    }

}
