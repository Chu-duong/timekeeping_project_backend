package com.aibou.security.controller;

import com.aibou.security.dto.request.CreateTimekeepingDto;
import com.aibou.security.dto.request.UpdateDayoffRequest;
import com.aibou.security.dto.response.DayOffResponse;
import com.aibou.security.entity.DayOff;
import com.aibou.security.repo.DayOffRepository;
import com.aibou.security.service.DayOffService;
import com.aibou.security.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DayOffController {
    @Autowired
    DayOffService dayOffService;
    @GetMapping("/day_off")
    public List<DayOffResponse> getAll()
    {
        return dayOffService.getAllDayOff();
    }
    @PostMapping("/day_off")
    public  ResponseEntity <?> addDayoff(@RequestBody DayOffResponse request)
    {
        return new ResponseEntity<>(dayOffService.addDayoff(request), HttpStatus.OK);
    }
    @PutMapping("/day_off")
    public  ResponseEntity <?> UpdateState(@RequestBody UpdateDayoffRequest request)
    {
        return new ResponseEntity<>(dayOffService.updateState(request), HttpStatus.OK);
    }
}
