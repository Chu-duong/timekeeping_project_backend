package com.aibou.security.controller;

import com.aibou.security.dto.request.CheckOutTimeRequest;
import com.aibou.security.dto.request.CreateTimekeepingDto;
import com.aibou.security.dto.response.TimeKeepingResponse;
import com.aibou.security.dto.response.UserResponse;
import com.aibou.security.repo.TimeKeepingRepo;
import com.aibou.security.service.TimeKeepingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class TimeKeepingController {
    @Autowired
    TimeKeepingService timeKeepingService;

    @GetMapping("/Detail/{id}")
    public ResponseEntity<?> detailTimeKeepingResponse(@PathVariable String id)
    {
        return new ResponseEntity<>(timeKeepingService.getTimeKeepingResponse(id), HttpStatus.OK);

    }
    @PostMapping("/add")
    public ResponseEntity <?> addTimeKeeping(@RequestBody CreateTimekeepingDto request)
    {
        return new ResponseEntity<>(timeKeepingService.addTimeKeeping(request), HttpStatus.OK);
    }
    @PutMapping ("/add_checkout")
    public ResponseEntity <?> UpdateTimeKeeping(@RequestBody CheckOutTimeRequest request)
    {
       return new ResponseEntity<>(timeKeepingService.updateTimeKeeping(request),HttpStatus.OK);
    }
}
