package com.aibou.security.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "position")
public class Position {
    @Id
    @Column(name="postion_id",unique = true,nullable = false)
    private Long id;
    @Column(name="position_name")
    private String positionName;
    @OneToMany(mappedBy = "position")
    @Transient
    private List<Employee> employeeList=new ArrayList<>();

}
