package com.aibou.security.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @Column(name = "employee_id", unique = true, nullable = false)
    private String id;
    @Column(name = "employee_name")
    private String name;
    @ManyToOne()
    @JoinColumn(name = "position_id")
    private Position position;
    @JsonBackReference
    @OneToMany(mappedBy = "employee")
    private Set<TimeKeeping> timeKeeping;

}
