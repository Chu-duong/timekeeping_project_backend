package com.aibou.security.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "document")
public class Document {
    @Id
    @Column(name="document_id")
    private Long id;
    @Column(name="document_name")
    private String name;
    @OneToMany(mappedBy = "document")
    @Transient
    private List<DayOff> dayOffsList=new ArrayList<>();
}
