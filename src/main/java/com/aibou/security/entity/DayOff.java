package com.aibou.security.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "dayoff")
public class DayOff {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "dayoff_col",nullable = false)
    private Date dayOff;
    @Column(name = "state", nullable = false)
    private boolean state;
    @Column(name = "content")
    private String content;
    @ManyToOne()
    @JoinColumn(name = "document_id")
    private Document document;
    @ManyToOne()
    @JoinColumn(name = "employee_id")
    private Employee employee;
 }
