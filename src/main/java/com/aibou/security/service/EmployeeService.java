package com.aibou.security.service;
import com.aibou.security.dto.response.UserResponse;
import com.aibou.security.entity.Employee;
import com.aibou.security.repo.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service

public class EmployeeService {
    @Autowired EmployeeRepository employeeRepo;

    public List<Employee> getEmployeeAll() {
        return employeeRepo.findAll();
    }
    public UserResponse getEmployee(String id) {

        Optional<Employee> employee=employeeRepo.findById(id);
        return  new UserResponse(employee.get());
    }
}
