package com.aibou.security.service;

import com.aibou.security.dto.request.UpdateDayoffRequest;
import com.aibou.security.dto.response.DayOffResponse;
import com.aibou.security.entity.DayOff;
import com.aibou.security.entity.TimeKeeping;
import com.aibou.security.exception.NotFoundException;
import com.aibou.security.repo.DayOffRepository;
import com.aibou.security.repo.DocumentRepository;
import com.aibou.security.repo.EmployeeRepository;
import com.aibou.security.repo.TimeKeepingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DayOffService {
    @Autowired
    DayOffRepository dayOffRepository;
    @Autowired
    TimeKeepingRepo timeKeepingRepo;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    DocumentRepository documentRepository;

    public List<DayOffResponse> getAllDayOff() {
        List<DayOff> dayOffs = dayOffRepository.findAll();
        List<DayOffResponse> dayOffArrayList = new ArrayList<>();
        for (DayOff dayOff : dayOffs) {
            dayOffArrayList.add((new DayOffResponse(dayOff)));
        }
        return dayOffArrayList;
    }

    public DayOff addDayoff(DayOffResponse entity) {
        DayOff dayOff;
        dayOff = DayOff.builder().
                dayOff(entity.getDate())
                .content(entity.getContent())
                .document(documentRepository.findById(entity.getDocument_id()).orElseThrow(() -> {
                    throw new NotFoundException("not found document");
                }))
                .state(false)
                .employee(employeeRepository.findById(entity.getEmployeeId()).orElseThrow(() -> {
                    throw new NotFoundException("not found user");
                })).build();
        DayOff d1 = dayOffRepository.save(dayOff);
        return d1;
    }

   public DayOff updateState( UpdateDayoffRequest request)
   {
       DayOff dayoff= dayOffRepository.findById(request.getId()).orElseThrow(() -> {
           throw new NotFoundException("Not found User");
       });
       dayoff.setState(true);
       DayOff d1=dayOffRepository.save(dayoff);
       return  d1;
   }

}
