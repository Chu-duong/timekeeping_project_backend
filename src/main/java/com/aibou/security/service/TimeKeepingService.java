package com.aibou.security.service;

import com.aibou.security.dto.request.CheckOutTimeRequest;
import com.aibou.security.dto.request.CreateTimekeepingDto;
import com.aibou.security.dto.response.TimeKeepingResponse;
import com.aibou.security.entity.DayOff;
import com.aibou.security.entity.Employee;
import com.aibou.security.entity.TimeKeeping;
import com.aibou.security.exception.BadRequestException;
import com.aibou.security.exception.NotFoundException;
import com.aibou.security.repo.DayOffRepository;
import com.aibou.security.repo.EmployeeRepository;
import com.aibou.security.repo.TimeKeepingRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TimeKeepingService {
    @Autowired
    private final EmployeeService employeeService;
    @Autowired
    private final EmployeeRepository employeeRepository;

    @Autowired
    private final TimeKeepingRepo timeKeepingRepo;
    @Autowired
    private final DayOffRepository dayOffRepository;

    String now = java.time.LocalDate.now().toString();

    public List<TimeKeepingResponse> getTimeKeepingResponse(String employeeId) {
        List<TimeKeeping> timeKeepings;
        List<DayOff> dayOffs = dayOffRepository.findDayOffByEmployee_Id(employeeId);
        timeKeepings = timeKeepingRepo.findTimeKeepingsByEmployee_Id(employeeId);
        ArrayList<TimeKeepingResponse> timeKeepingResponseArrayList = new ArrayList<>();
        for (int i = 1; i <= LocalDate.now().getDayOfMonth(); i++) {
            String inputDate = LocalDate.now().getYear() + "-" + LocalDate.now().getMonthValue() + "-" + i;
            LocalDate localDate = LocalDate.parse(inputDate, DateTimeFormatter.ofPattern("yyyy-M-d"));
            String date = localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            TimeKeeping tk = new TimeKeeping();
            if (timeKeepingRepo.getDayCheckin(date, employeeId) != null) {
                tk = timeKeepingRepo.getDayCheckin(date, employeeId);
                timeKeepingResponseArrayList.add(new TimeKeepingResponse(tk));
            } else {
                tk.setEmployee(employeeRepository.findById(employeeId).orElseThrow(() -> {
                    throw new NotFoundException("not found Employee");
                }));
                timeKeepingResponseArrayList.add(new TimeKeepingResponse(tk, date));
            }
        }
        for (DayOff dayOff : dayOffs) {
            for (TimeKeepingResponse timeKeeping : timeKeepingResponseArrayList) {
                if (timeKeeping.getState() != null) {
                    if (dayOff.getDayOff().toString().substring(0, 10).trim().compareTo(timeKeeping.getDate().toString().substring(0, 10)) == 0 &&
                            dayOff.isState()) {
                        if (timeKeeping.getState().equals("nghi khong phep") && dayOff.getDocument().getId() == 3) {
                            timeKeeping.setState("nghi co phep");
                        }
                        if (timeKeeping.getState().equals(" ve som") && dayOff.getDocument().getId() == 2) {
                            timeKeeping.setState("");
                        }
                        if (timeKeeping.getState().equals("di muon") && dayOff.getDocument().getId() == 1) {
                            timeKeeping.setState("");
                        }
                        if (timeKeeping.getState().equals("di muon ve som") && dayOff.getDocument().getId() == 1) {
                            timeKeeping.setState("ve som");
                        }
                        if (timeKeeping.getState().equals("di muon ve som") && dayOff.getDocument().getId() == 2) {
                            timeKeeping.setState("di muon");
                        }
                    }
                }
            }
        }
        return timeKeepingResponseArrayList;

    }

    public TimeKeeping addTimeKeeping(CreateTimekeepingDto request) {
        TimeKeeping timeKeeping;
        String time=request.getCheckIn().toString().substring(11, 19);
        String endTime="18:00:00";
        if(time.compareTo(endTime)>0)
        {
            throw  new BadRequestException("Check in khong duoc phep sau 18h");
        }
        timeKeeping = TimeKeeping.builder().
                checkIn(request.getCheckIn()).
                employee(employeeRepository.findById(request.getEmployee()).orElseThrow(() -> {
                    throw new NotFoundException("not found Employee");
                }))
                .build();
        TimeKeeping t1 = timeKeepingRepo.save(timeKeeping);
        return t1;
    }

    public TimeKeeping updateTimeKeeping(CheckOutTimeRequest request) {
        String date = request.getDate().substring(0, 10);
        String time = request.getDate().substring(11, 19);
        String startTime = time;
        String endTime = "9:00:00";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date d1 = null;
        try {
            d1 = sdf.parse(startTime);
            Date d2 = sdf.parse(endTime);
            long elapsed = d2.getTime() - d1.getTime();
            if (elapsed > 0) {
                throw new BadRequestException("Check out khong duoc phep 9h");
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        TimeKeeping timeKeeping = timeKeepingRepo.getDayCheckin(date, request.getId());
        if (timeKeeping == null) {
            throw new NotFoundException("you have to check in first");
        }
        String time2 = timeKeeping.getCheckIn().toString().substring(11, 19);

        if (time.compareTo(time2) <= 0) {
            throw new BadRequestException("Check out khong duoc truoc thoi gian check in");
        }
        timeKeeping.setCheckOut(request.getCheckOut());
        timeKeepingRepo.save(timeKeeping);
        return timeKeeping;
    }
}
