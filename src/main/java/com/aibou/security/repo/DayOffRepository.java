package com.aibou.security.repo;

import com.aibou.security.entity.DayOff;
import jakarta.persistence.Id;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.yaml.snakeyaml.events.Event;

import java.util.List;

@Repository
public interface DayOffRepository extends JpaRepository<DayOff, Long> {
    List<DayOff> findDayOffByEmployee_Id(String id);
}
