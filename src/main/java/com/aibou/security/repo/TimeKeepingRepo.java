package com.aibou.security.repo;

import com.aibou.security.entity.TimeKeeping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface TimeKeepingRepo extends JpaRepository<TimeKeeping,Long> {

    List<TimeKeeping> findTimeKeepingsByEmployee_Id(String id);

    @Query(value = "select * from timekeeping where check_in like %?1% and employee_id = ?2", nativeQuery = true)
    TimeKeeping getDayCheckin(String date, String eid);
    @Query(value = "select * from timekeeping where check_out like %?1% and employee_id = ?2", nativeQuery = true)
    TimeKeeping getDayCheckOut(String date, String eid);
}
