package com.aibou.security.repo;

import com.aibou.security.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public  interface EmployeeRepository extends JpaRepository<Employee,String> {

}
