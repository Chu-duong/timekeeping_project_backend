package com.aibou.security.dto.request;

import com.aibou.security.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateTimekeepingDto {
    private Long id;
    private Date checkIn;
    private Date checkOut;
    private String employee;

}
