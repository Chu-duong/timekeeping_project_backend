package com.aibou.security.dto.response;

import com.aibou.security.entity.DayOff;
import com.aibou.security.entity.Position;
import com.aibou.security.entity.TimeKeeping;
import com.aibou.security.repo.TimeKeepingRepo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TimeKeepingResponse {
    private Long id;
    private String employeeName;
    private String CheckIn;
    private String CheckOut;
    private Position position;
    private String employeeId;
    private String state;
    private String date;
    private DayOff dayOff;

    String now=java.time.LocalDate.now().toString();

    public TimeKeepingResponse(TimeKeeping entity,String date) {
        if (entity.getCheckIn()==null&&entity.getCheckOut()==null) {
            this.setCheckIn("--");
            this.setCheckOut("--");
            this.state="nghi khong phep";
            this.employeeId = entity.getEmployee().getId();
            this.employeeName = entity.getEmployee().getName();
            this.date=date;
            if(date.compareTo(now)==0){;
                this.state="";
        }
    }}
    public TimeKeepingResponse(TimeKeeping entity) {
            String temp = "";
            this.id = entity.getId();

            this.employeeId = entity.getEmployee().getId();
            this.employeeName = entity.getEmployee().getName();
            this.position = entity.getEmployee().getPosition();
            String[] check_in = entity.getCheckIn().toString().split(" ");
            this.CheckIn = check_in[1].trim().substring(0,8);
            this.date = check_in[0];
            LocalTime time1 = LocalTime.parse(check_in[1].substring(0, 8));

            LocalTime checkIn_time = LocalTime.parse("08:00:00");

            if (time1.compareTo(checkIn_time) > 0) {
                temp = "di muon";
                this.state = temp;
            }
            this.CheckOut="--";
            if (entity.getCheckOut() != null) {
                String[] check_out = entity.getCheckOut().toString().split(" ");
                this.CheckOut = check_out[1].trim().substring(0,8);
                LocalTime time2 = LocalTime.parse(check_out[1].substring(0, 8));
                LocalTime checkIn_out = LocalTime.parse("17:00:00");
                if (time2.compareTo(checkIn_out) < 0) {
                    temp += " ve som";
                    this.state = temp;
                }
            }
            if (entity.getCheckIn() != null && entity.getCheckOut() == null && check_in[0].compareTo(now) < 0) {
                this.state = "nghi khong phep";
            }

        }
    }

