package com.aibou.security.dto.response;

import com.aibou.security.entity.DayOff;
import com.aibou.security.entity.Position;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TimeKeepingResponseImp {
    private Long id;
    private String employeeName;
    private String CheckIn;
    private String CheckOut;
    private Position position;
    private String employeeId;
    private String state;
    private String date;
    private Set<DayOff> dayOff;


}
