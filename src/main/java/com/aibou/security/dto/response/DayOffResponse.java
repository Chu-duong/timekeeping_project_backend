package com.aibou.security.dto.response;

import com.aibou.security.entity.DayOff;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DayOffResponse {
    private Long id;
    private Date date ;
    private String state;
    private String content;
    private String applicationName;
    private String employeeName;
    private String employeeId;
    private  Long document_id;
    public DayOffResponse(DayOff entity)
    {
        this.id=entity.getId();
        this.date= entity.getDayOff();
        this.state="New";
        this.content=entity.getContent();
        this.applicationName=entity.getDocument().getName();
        this.employeeName=entity.getEmployee().getName();
        this.employeeId=entity.getEmployee().getId();
if(entity.isState())
{
    this.state="approved";
}
    }

}
