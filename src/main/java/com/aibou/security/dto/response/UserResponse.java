package com.aibou.security.dto.response;

import com.aibou.security.entity.Employee;
import com.aibou.security.entity.Position;
import com.aibou.security.entity.TimeKeeping;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class UserResponse {
private String id;
private String name;
private Position position;
private Set<TimeKeepingResponse> timeKeepingResponseSet;
public UserResponse(Employee entity)
{
    this.id=entity.getId();
    this.name=entity.getName();
    this.position=entity.getPosition();
    if(entity.getTimeKeeping()!= null)
    {
        Set<TimeKeepingResponse> set= new HashSet<>();
        for(TimeKeeping tk: entity.getTimeKeeping())
        {
            try{
                set.add(new TimeKeepingResponse(tk));
            }
           catch (Exception ex)
           {
               System.out.println(ex);
           }
        }
        this.timeKeepingResponseSet=set;
    }
}
}
